<?php
require_once "vendor/autoload.php";

use GuzzleHttp\Client;

class Telegram
{
    public $base_uri = "https://api.telegram.org";
    protected $bot_token = '';
    protected $client;

    function __construct($bot_token)
    {
        $this->bot_token = $bot_token;
        $this->client = $client = new Client([
            // Base URI is used with relative requests
            "base_uri" => $this->base_uri,
        ]);
    }

    function getChannels()
    {
        try {

            $response = $this->client->request("GET", "/bot$this->bot_token/getUpdates");

            $body = $response->getBody();
            $arr_body = json_decode($body);

            if (!($arr_body->ok)) {
                throw new Exception("The API token is invalid.");
            }

            if (empty($arr_body->result)) {
                throw new Exception("Please add this bot to a Telegram group or channel and promote as an admin.");
            }

            $arr_result = array();
            foreach ($arr_body->result as $result) {
                if (!empty($result->my_chat_member)) {
                    $arr_result[] = [
                        'chat_id' => $result->my_chat_member->chat->id,
                    ];
                }
            }

            return $arr_result;

        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function sendMessage($message)
    {
        $channels = self::getChannels();
        try {
            foreach ($channels as $channel) {
                $this->client->request("GET", "/bot$this->bot_token/sendMessage", [
                    "query" => [
                        "chat_id" => $channel['chat_id'],
                        "text" => $message
                    ]
                ]);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}

